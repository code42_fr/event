$(document).ready(function() {

$('#formevent').submit(function(){
  var ok = 0;
  var ok_change = 0;
  var nb_unit = parseInt($('#nb_unit').text());
  var cout_total = parseInt($('#totval').text());
  $('input[type=checkbox]').each(function () {
             if (this.checked) {
                    ok = 1;
             }
             if ($(this).hasClass("change")){
               ok_change = 1;
             }
  });
  if ((ok == 1 && ok_change == 1) || ok_change == 1){
    $('.send_event').prop('disabled', false);
    $('.send_event').css('cursor', 'pointer');
    $('.send_error').hide("fast");
  }
    else {
      $('.send_event').prop('disabled', true);
      $('.send_event').css('cursor', 'not-allowed');
      $('.send_error').show("fast");
      return false;
    }
    if (cout_total > nb_unit)
    {
      $('#send_event_top').val('PAS ASSEZ D\'UNITES');
      $('#send_event_bot').val('PAS ASSEZ D\'UNITES');
      $('#send_event').prop('disabled', true);
      $('#send_event').prop('disabled', true);
      return false;
    }
    else{
      $('#send_event_top').val('RÉSERVEZ VOS COURS');
      $('#send_event_bot').val('RÉSERVEZ VOS COURS');
      $('#send_event').prop('disabled', false);
      $('#send_event').prop('disabled', false);
    }
});



$(':checkbox').change(function() {
var ok = 0;
var ok_change = 0;
var cout_total = 0;
var nb_unit = parseInt($('#nb_unit').text());
if ($(this).hasClass("change"))
$(this).removeClass("change")
else
$(this).addClass("change");

$('input[type=checkbox]').each(function () {
           if (this.checked && !($(this).hasClass("payed"))) {
                  ok = 1;
                  cout_total += parseInt($(this).attr("data-price"));
           }
          if ($(this).hasClass("change")){
            ok_change = 1;
          }

});

$(".totalvalue").text(cout_total);

if ((ok == 1 && ok_change == 1) || ok_change == 1){
  $('.send_event').prop('disabled', false);
  $('.send_event').css('cursor', 'pointer');
  $('.send_error').hide("fast");
}
  else {
    $('.send_event').prop('disabled', true);
    $('.send_event').css('cursor', 'not-allowed');
    $('.send_error').show("fast");
  }

  if (cout_total > nb_unit)
  {
    $('#send_event_top').val('PAS ASSEZ D\'UNITES');
    $('#send_event_bot').val('PAS ASSEZ D\'UNITES');
  }
  else{
    $('#send_event_top').val('RÉSERVEZ VOS COURS');
    $('#send_event_bot').val('RÉSERVEZ VOS COURS');
  }

});
});
